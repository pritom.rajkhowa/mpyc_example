"""
Unanimous voting between parties P[0],...,P[t] is implemented by securely
evaluating the product of their votes (using 1s and 0s to encode "yes"
and "no" votes, respectively) and revealing only whether the product
equals 1 (unanimous agreement) or 0 (someone disagrees). The MPyC method
mpc.all() modeled after Python's built-in function all() can be used
for this purpose.

The secure computation is designed to be maximally private, meaning
that any t parties colluding against the remaining party should be
unsuccessful: the colluding parties should not be able to find out
the vote of the remaining party in case they do not agree (and no
unanimous agreement will be reached anyway; if the colluding parties
all vote "yes", then there remains nothing to hide).
"""

import sys
import json
import os
from mpyc.runtime import mpc

m = len(mpc.parties)

if m%2 == 0:
    print('Odd number of parties required.')
    sys.exit()

t = m//2

voters = list(range(t+1))  # parties P[0],...,P[t]

if mpc.pid in voters:
    vote = int(sys.argv[1]) if sys.argv[1:] else 1  # default "yes"
else:
    vote = None  # no input


secbit = mpc.SecFld(2)  # secure bits over GF(2)

mpc.run(mpc.start())
votes = mpc.input(secbit(vote), senders=voters)
result = mpc.run(mpc.output(mpc.all(votes), receivers=voters))
mpc.run(mpc.shutdown())


if result is not None:  # no output check

    #open config file
    with open('../result/result.json', 'r') as f:
        config = json.load(f)

    #edit the data

    print('%%%%%%%%%%^^^^^^^^^^^^^^^####################')
    print(result)
    print('%%%%%%%%%%^^^^^^^^^^^^^^^####################')

    if result:

        config['Final_Result_Message']= f' Unanimous agreement reached from {t+1} part{"ies" if t else "y"}!'

    else:

        config['Final_Result_Message']= f' Someone disagrees among {t+1} part{"ies" if t else "y"}!'  

    vote_values=[]

    for vote in votes:
        vote_values.append(str(vote))


    config["Voter_List"]=vote_values  


    #write it back to the file
    with open('../result/result.json', 'w') as f:
        json.dump(config, f)