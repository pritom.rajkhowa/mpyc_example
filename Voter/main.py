import streamlit as st
import json
import os
import subprocess

st.header('Secure Multiparty Secure Voting System using MPyC(Voter)')

vote=st.selectbox('Cast Your Vote',['Yes','No'])

if st.button("Submit Your Secure Vote"):

	st.text("Congratulations ! Successfully Submitted Your Secure Vote")

	#open config file
	with open('../config/config.json', 'r') as f:
		config = json.load(f)

	voter_id = int(config['submitted_vote'])

	voter_no = int(config['total_voter'])

	#edit the data
	config['submitted_vote'] = int(config['submitted_vote'])+1
	

	#write it back to the file
	with open('../config/config.json', 'w') as f:
		json.dump(config, f)

	if vote=='Yes':

		vote_bool=1

	else:

		vote_bool=0

	shell_cmd = ["python3", "../processfile/securevote_mpc.py","-M"+str(voter_no),"-I"+str(voter_id),str(vote_bool)]

	print(vote)

	print(shell_cmd)

	process = subprocess.run(shell_cmd, capture_output=True, encoding="utf-8")

	result_process=process.stdout

	#print(result_process)
	#print('--------------------------------------xxxxxxx-----------------------------')



