# MPyC_Example

## Introduction

Ballot secrecy is an essential goal in the design of voting systems, since when voters are concerned for their privacy, they might decide to vote differently from their real preferences, or even abstain from voting altogether. The usual meaning of privacy in the context of secure voting is that the voters remain anonymous. Namely, even though the ballots are known (as is the case when opening the ballot box at the end of an election day), no ballot can be traced back to the voter who cast it.

## Goal

In this example, I try to demonstrate that the secure electronic voting protocol , based on secure multiparty computation (MPC).


## Proposed model to achieve the goal

Secure voting between parties P[0],...,P[t] is implemented by securely evaluating the product of their votes (using 1s and 0s to encode "yes" and "no" votes, respectively) and revealing only whether the product equals 1 (unanimous agreement) or 0 (someone disagrees). 

To achieve maximal privacy, we add t parties P[t+1],...,P[2t] to the secure computation. These additional parties provide no input to the secure computation and are trusted not to collude with any of the parties P[0],...,P[t]. This way we have m=2t+1 parties in total, and we can tolerate the desired maximum of t corrupt parties among P[0],...,P[t]. Moreover, the trusted parties do not receive any output, so they do not learn anything at all, as their number does not exceed t.



## Prerequisite packages

### Python3 

### pip install mpyc (MPyC Secure Multiparty Computation in Python)

### pip install streamlit (Streamlit is an open source python based framework for developing and deploying interactive.)​

### pip install pandas (pandas: powerful Python data analysis toolkit




## How to start Administrator 

Go to Admin folder and run the following command:  

```
streamlit run main.py
```

## How to start Voter

Go to Voter folder and run the following command:  

```
streamlit run main.py
```



## Contract: pritom.rajkhowa [at] gmail.com



