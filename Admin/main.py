import streamlit as st
import pandas as pd
import json
import os
import subprocess

st.header('Secure Multiparty Secure Voting System using MPyC(Administrator)')

voter_no = st.number_input('Pick the voter number', 2,10)

if st.button("Start Secure Voting System"):


	#open config file
	with open('../config/config.json', 'r') as f:
		config = json.load(f)

	#edit the data
	config['total_voter'] = voter_no+1

	config['submitted_vote'] = 0

	#write it back to the file
	with open('../config/config.json', 'w') as f:
		json.dump(config, f)



	#open config file
	with open('../result/result.json', 'r') as f:
		config = json.load(f)

	#edit the data
	config['Final_Result_Message'] = ''

	config['Voter_List'] = []

	#write it back to the file
	with open('../result/result.json', 'w') as f:
		json.dump(config, f)



	st.text("Successfully Updated Configuration for "+str(voter_no)+" Voters")

	shell_cmd = ["python3", "../processfile/securevote_mpc.py","-M"+str(voter_no+1),"-I0"]

	print(shell_cmd)

	#open config file
	with open('../config/config.json', 'r') as f:
		config = json.load(f)

	#edit the data
	config['submitted_vote'] = int(config['submitted_vote'])+1


	#write it back to the file
	with open('../config/config.json', 'w') as f:
		json.dump(config, f)

	process = subprocess.run(shell_cmd, capture_output=True, encoding="utf-8")

	result_process=process.stdout

	#print(process.stdout)

	#open config file
	with open('../result/result.json', 'r') as f:
		config = json.load(f)

    #get the data
	output_result=config['Final_Result_Message']

	votes=config["Voter_List"]


	voter_ids=[]

	vote_values=[]

	for vote in votes:

		vote_values.append(vote)

		voter_ids.append(len(vote_values))


	df = pd.DataFrame(vote_values,columns=['Casted Vote'])

	if output_result.strip()!="":

		st.text(output_result)

		if len(vote_values)>0:

			st.table(df)












